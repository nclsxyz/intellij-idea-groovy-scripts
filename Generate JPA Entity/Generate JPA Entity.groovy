import com.intellij.database.model.*
import com.intellij.database.util.Case
import com.intellij.database.util.DasUtil

// A mapping from database column types to Java data types.
typeMapping = [
        (~/(?i)int/)                      : "long",
        (~/(?i)float|double|decimal|real/): "double",
        (~/(?i)datetime|timestamp/)       : "java.sql.Timestamp",
        (~/(?i)date/)                     : "java.sql.Date",
        (~/(?i)time/)                     : "java.sql.Time",
        (~/(?i)/)                         : "String"
]

// Ask the user to choose a directory to save generated Java classes.
FILES.chooseDirectoryAndSave("Choose directory", "Choose where to store generated files") { dir ->
    // Deduce the Java package name from the chosen directory path.
    packageName = dir.path.replaceAll(".*src[/\\\\]", "").replaceAll("[/\\\\]", ".") + ";"
    // For each selected table, generate a corresponding Java class.
    SELECTION.filter { it instanceof DasTable }.each { generate(it, dir) }
}

/**
 * Generate the Java class for the given table and save it to the specified directory.
 * @param table The table to generate a Java class for.
 * @param dir The directory to save the Java class.
 */
def generate(table, dir) {
    def className = javaName(table.getName(), true)
    def fields = calcFields(table)
    new File(dir, className + ".java").withPrintWriter { out -> generateClass(out, className, fields, table.getName()) }
}

/**
 * Write the Java class content to the specified writer.
 * @param out The writer to write the Java class content to.
 * @param className The name of the Java class.
 * @param fields The fields (columns) of the class.
 * @param tableName The name of the database table.
 */
def generateClass(out, className, fields, tableName) {
    out.println "package $packageName;"
    out.println ""
    out.println "import javax.persistence.*;"
    out.println ""
    out.println "@Entity"
    out.println "@Table(name = \"$tableName\")"
    out.println "public class $className {"
    out.println ""
    fields.each() {
        if (it.annos != "") out.println "  ${it.annos}"
        out.println "  private ${it.type} ${it.name};"
    }
    out.println ""
    fields.each() {
        out.println ""
        out.println "  public ${it.type} get${it.name.capitalize()}() {"
        out.println "    return ${it.name};"
        out.println "  }"
        out.println ""
        out.println "  public void set${it.name.capitalize()}(${it.type} ${it.name}) {"
        out.println "    this.${it.name} = ${it.name};"
        out.println "  }"
        out.println ""
    }
    out.println "}"
}

/**
 * Calculate the fields for the given table.
 * @param table The table to calculate fields for.
 * @return A list of fields (columns) for the table.
 */
def calcFields(table) {
    DasUtil.getColumns(table).reduce([]) { fields, col ->
        def spec = Case.LOWER.apply(col.getDataType().getSpecification())
        def typeStr = typeMapping.find { p, t -> p.matcher(spec).find() }.value
        def annos = ""

        if (DasUtil.isPrimary(col)) {
            annos += "@Id\n  "
        }

        if (col.isNotNull()) {
            annos += "@Column(name = \"${col.getName()}\", nullable = false)"
        } else {
            annos += "@Column(name = \"${col.getName()}\")"
        }

        fields += [[
                           name : javaName(col.getName(), false),
                           type : typeStr,
                           annos: annos]]
    }
}

/**
 * Convert the given string to a valid Java name.
 * @param str The string to convert.
 * @param capitalize Whether to capitalize the first letter.
 * @return A valid Java name.
 */
def javaName(str, capitalize) {
    def s = com.intellij.psi.codeStyle.NameUtil.splitNameIntoWords(str)
            .collect { Case.LOWER.apply(it).capitalize() }
            .join("")
            .replaceAll(/[^\p{javaJavaIdentifierPart}[_]]/, "_")
    capitalize || s.length() == 1? s : Case.LOWER.apply(s[0]) + s[1..-1]
}
